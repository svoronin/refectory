package ua.pp.voronin.sergii.softserve.test;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.beans.factory.annotation.Autowired;
import ua.pp.voronin.sergii.softserve.test.model.Product;
import ua.pp.voronin.sergii.softserve.test.model.RefectoryNetwork;
import ua.pp.voronin.sergii.softserve.test.service.NetworkService;
import ua.pp.voronin.sergii.softserve.test.service.ProductService;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Collections;

@Path("/refectory/")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class RefectoryNetworkController {

    private NetworkService networkService;
    private ProductService productService;

    @Autowired
    public RefectoryNetworkController(NetworkService networkService,
                                      ProductService productService) {
        this.networkService = networkService;
        this.productService = productService;
    }

    @GET
    @Path("/networks")
    public Collection<RefectoryNetwork> getNetworks() {
        return Collections.singleton(networkService.createNetwork());
    }

    // TODO Handle exceptions to avoid full stacktrace in logs
    @PUT
    @Path("/product")
    public Response addProduct(Product product) throws URISyntaxException {
        Product existingProduct = productService.findProductByName(product.getName());
        if (existingProduct == null) {
            product = productService.saveProduct(product);
            return Response.created(new URI("/refectory/product/" + product.getId())).entity(product).build();
        } else {
            return Response.status(Response.Status.CONFLICT).location(new URI("/refectory/product/" + existingProduct.getId())).entity(existingProduct).build();
        }
    }

    @GET
    @Path("/product/{id}")
    public Response getProductById(@PathParam("id") Integer id) {
        Product match = productService.findProductById(id);
        if (match != null) {
            return Response.ok(match).build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @GET
    @Path("/product")
    public Response getProduct(@QueryParam("id") Integer id,
                               @QueryParam("name") String name) {
        if (id != null) {
            Product match = productService.findProductById(id);
            if (match != null) {
                return Response.ok(match).build();
            }
        }
        if (name != null && !name.isEmpty()) {
            Product match = productService.findProductByName(name);
            if (match != null) {
                return Response.ok(match).build();
            }
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @POST
    @Path("/product/{id}")
    public Response updateProduct(@PathParam("id") Integer id,
                                  Product product) {
        Product existingProduct = productService.findProductById(id);
        if (existingProduct != null) {
            if(product.getId() == null) {
                product.setId(id);
            } else if (product.getId() != id) {
                return Response.status(Response.Status.BAD_REQUEST).entity(new Error().setReason("Ids mismatch")).build();
            }
            product = productService.saveProduct(product);
            return Response.ok(product).build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @DELETE
    @Path("/product/{id}")
    public Response deleteProductById(@PathParam("id") Integer id) {
        Product match = productService.findProductById(id);
        if (match != null) {
            productService.deleteProduct(match);
            return Response.ok(match).build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @Getter
    @Setter
    @Accessors(chain = true)
    private static class Error {
        private String reason;
    }

//    @POST
//    @Path("/addDish")
//    public Response addDish(Dish dish) {
//        return Response.accepted(productService.create(dish)).build();
//    }
}
