package ua.pp.voronin.sergii.softserve.test;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;
import org.springframework.web.context.ContextLoaderListener;
import ua.pp.voronin.sergii.softserve.test.config.RestaurantResourceConfig;

public class RefectoryRunner {

    private static final int PORT = 8080;

    public static void main(String[] args) throws Exception {
        ServletContextHandler contextHandler = new ServletContextHandler(ServletContextHandler.SESSIONS);
        contextHandler.setContextPath("/");

        Server server = new Server(PORT);
        server.setHandler(contextHandler);

        ServletHolder servletHolder = contextHandler.addServlet(ServletContainer.class, "/*");
        servletHolder.setInitOrder(0);
        contextHandler.setInitParameter("contextConfigLocation", "classpath:applicationContext.xml");
        contextHandler.addEventListener(new ContextLoaderListener());
        servletHolder.setInitParameter("javax.ws.rs.Application", RestaurantResourceConfig.class.getCanonicalName());

        try {
            server.start();
            server.join();
        } finally {
            server.stop();
        }
    }
}
