package ua.pp.voronin.sergii.softserve.test.config;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.web.filter.RequestContextFilter;
import ua.pp.voronin.sergii.softserve.test.RefectoryNetworkController;

public class RestaurantResourceConfig extends ResourceConfig {

    public RestaurantResourceConfig() {
        register(RequestContextFilter.class);
        register(RefectoryNetworkController.class);
    }
}
