package ua.pp.voronin.sergii.softserve.test.dao;

import ua.pp.voronin.sergii.softserve.test.model.Product;

import java.util.List;

public interface ProductDao {

    Product saveProduct(Product product);

    List<Product> findProductById(int id);

    List<Product> findProductByName(String name);

    void deleteProduct(Product product);
}
