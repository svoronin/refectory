package ua.pp.voronin.sergii.softserve.test.dao.impl;

import org.springframework.beans.factory.annotation.Required;
import ua.pp.voronin.sergii.softserve.test.dao.ProductDao;
import ua.pp.voronin.sergii.softserve.test.model.Product;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class ProductDaoImpl implements ProductDao {

    private EntityManager entityManager;

    @Required
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public Product saveProduct(Product product) {
        try {
            entityManager.getTransaction().begin();
            product = entityManager.merge(product);
            entityManager.getTransaction().commit();
            return product;
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        }
    }

    @Override
    public List<Product> findProductById(int id) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Product> preQuery = criteriaBuilder.createQuery(Product.class);
        Root<Product> productRoot = preQuery.from(Product.class);
        CriteriaQuery<Product> query = preQuery.select(productRoot).where(criteriaBuilder.equal(productRoot.get("id"), id));
        return entityManager.createQuery(query).getResultList();
    }

    @Override
    public List<Product> findProductByName(String name) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Product> preQuery = criteriaBuilder.createQuery(Product.class);
        Root<Product> productRoot = preQuery.from(Product.class);
        CriteriaQuery<Product> query = preQuery.select(productRoot).where(criteriaBuilder.equal(productRoot.get("name"), name));
        return entityManager.createQuery(query).getResultList();
    }

    @Override
    public void deleteProduct(Product product) {
        try {
            entityManager.getTransaction().begin();
            entityManager.remove(product);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
        }
    }
}
