package ua.pp.voronin.sergii.softserve.test.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Address {

    private String city;
    private String street;
    private String number;

}
