package ua.pp.voronin.sergii.softserve.test.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Cook extends Employee {

    private byte experience;
    private boolean certified;

}
