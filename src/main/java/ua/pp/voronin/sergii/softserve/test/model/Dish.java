package ua.pp.voronin.sergii.softserve.test.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Collection;

@Entity
@Getter
@Setter
public class Dish {

    @Id
    @GeneratedValue
    private Integer id;
    private String name;
    private Float price;
    private String description;
    private byte[] picture;
    @OneToMany(cascade = CascadeType.ALL)
    private Collection<Product> products;

}
