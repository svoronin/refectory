package ua.pp.voronin.sergii.softserve.test.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class Employee {

    private String name;
    private String surname;
    private byte age;
    private Gender gender;
    private Refectory work;

}
