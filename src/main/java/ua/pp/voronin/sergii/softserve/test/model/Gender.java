package ua.pp.voronin.sergii.softserve.test.model;

public enum Gender {
    MAN,
    WOMAN,
    OTHER
}
