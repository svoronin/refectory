package ua.pp.voronin.sergii.softserve.test.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Guard extends Employee {

    private Shift shift;

}
