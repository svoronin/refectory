package ua.pp.voronin.sergii.softserve.test.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Collection;

@Getter
@Setter
public abstract class Refectory {

    private String name;
    private Address address;
    private Collection<Employee> staff;
    private RefectoryCategory category;
    private byte capacity;
    private Menu menu;

}
