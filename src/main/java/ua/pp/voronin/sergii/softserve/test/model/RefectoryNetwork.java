package ua.pp.voronin.sergii.softserve.test.model;

import java.util.ArrayList;
import java.util.Collection;

public class RefectoryNetwork {

    private final String name;
    private Collection<Refectory> network = new ArrayList<>();

    public RefectoryNetwork(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean addRefectry(Refectory refectory) {
        return network.add(refectory);
    }

    public boolean removeRefectory(Refectory refectory) {
        return network.remove(refectory);
    }

    /**
     * Returns copy of the network. Use instance methods in case you need to
     * change internal structure.
     *
     * @return copy of network of refectories
     */
    public Collection<Refectory> getNetwork() {
        return new ArrayList<>(network);
    }
}
