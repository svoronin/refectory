package ua.pp.voronin.sergii.softserve.test.model;

public enum Shift {
    MORNING(3, 11),
    DAY(9, 17),
    EVENING(15, 23),
    NIGHT(21, 5);

    private byte start;
    private byte end;

    Shift(int start, int end) {
        this.start = (byte) start;
        this.end = (byte) end;
    }

    public byte getStart() {
        return start;
    }

    public byte getEnd() {
        return end;
    }
}
