package ua.pp.voronin.sergii.softserve.test.service;

import ua.pp.voronin.sergii.softserve.test.model.RefectoryNetwork;

public interface NetworkService {

    RefectoryNetwork createNetwork();
}
