package ua.pp.voronin.sergii.softserve.test.service;

import org.springframework.beans.factory.annotation.Autowired;
import ua.pp.voronin.sergii.softserve.test.dao.ProductDao;
import ua.pp.voronin.sergii.softserve.test.model.Product;

import java.util.List;

public class ProductService {

    private ProductDao productDao;

    @Autowired
    public ProductService(ProductDao productDao) {
        this.productDao = productDao;
    }

    public Product saveProduct(Product product) {
        return productDao.saveProduct(product);
    }

    public Product findProductById(int id) {
        List<Product> products = productDao.findProductById(id);
        if (products == null || products.isEmpty()) {
            return null;
        }
        return products.get(0);
    }

    public Product findProductByName(String name) {
        List<Product> products = productDao.findProductByName(name);
        if (products == null || products.isEmpty()) {
            return null;
        }
        return products.get(0);
    }

    public void deleteProduct(Product product) {
        productDao.deleteProduct(product);
    }
}
