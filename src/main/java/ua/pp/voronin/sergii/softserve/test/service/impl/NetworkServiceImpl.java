package ua.pp.voronin.sergii.softserve.test.service.impl;

import org.springframework.beans.factory.annotation.Required;
import ua.pp.voronin.sergii.softserve.test.model.RefectoryNetwork;
import ua.pp.voronin.sergii.softserve.test.service.NetworkService;

import javax.persistence.EntityManager;

public class NetworkServiceImpl implements NetworkService {

    private EntityManager entityManager;

    @Required
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public RefectoryNetwork createNetwork() {
        return new RefectoryNetwork("test");
    }
}
